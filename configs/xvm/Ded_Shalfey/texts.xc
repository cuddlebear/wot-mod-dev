﻿/**
 * Text substitutions.
 * Текстовые подстановки.
 */
{
  "texts": {
    // Text for {{vtype}} macro.
    // Текст для макроса {{vtype}}.
    "vtype": {
      // Text for light tanks     / Текст для легких танков.
      "LT":  "&#x3A;",
      // Text for medium tanks    / Текст для средних танков.
      "MT":  "&#x3B;",
      // Text for heavy tanks     / Текст для тяжелых танков.
      "HT":  "&#x3F;",
      // Text for arty            / Текст для арты.
      "SPG": "&#x2D;",
      // Text for tank destroyers / Текст для ПТ.
      "TD":  "&#x2E;"
    },
    // Text for {{gun-marks}} macro.
    // Текст для макроса {{gun-marks}}
    "marksOnGun": {
      "_0": "0",
      "_1": "1",
      "_2": "2",
      "_3": "3"
    },
    // Text for {{spotted}} macro.
    // Текст для макроса {{spotted}}
    "spotted": {
      "neverSeen": "",
      "lost": "<font face='xvm' size='23' color='#FFFFFF'>&#x70;</font>",
      "spotted": "<font face='xvm' size='23' color='#FF0000'>&#x70;</font>",
      "dead": "<font face='xvm' size='23' color='#FFFFFF'>&#x76;</font>",
      // Artillery specific values.
      // Специфичные значения для артиллерии.
      "neverSeen_arty": ${"texts.spotted.neverSeen"},
      "lost_arty":      ${"texts.spotted.lost"},
      "spotted_arty":  ${"texts.spotted.spotted"},
      "dead_arty":      ${"texts.spotted.dead"}
    }
  }
}
