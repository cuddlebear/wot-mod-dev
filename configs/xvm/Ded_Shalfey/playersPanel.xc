﻿/**
 * Parameters of the Players Panels ("ears").
 * Параметры панелей игроков ("ушей").
 */
{
  // Enemy spotted status marker format for substitutions in extra fields.
  // Подстановка для дополнительного поля с маркером статуса засвета
  "enemySpottedMarker": {
    // Opacity percentage of spotted markers in the panels. 0 - transparent (disabled) ... 100 - opaque.
    // Прозрачность в процентах маркеров засвета в ушах. 0 - полностью прозрачные (отключены), 100 - не прозрачные.
    "alpha": 100,
    // x position.
    // положение по горизонтали.
    "x": -4,
    // y position.
    // положение по вертикали.
    "y": 2,
    // true - x position is binded to vehicle icon, false - binded to edge of the screen.
    // true - положение по горизонтали отсчитывается от иконки танка, false - от края экрана.
    "bindToIcon": true,
    // enemy spotted status marker format.
    // формат маркера статуса засвета.
    "format": "{{spotted}}",
    // shadow (see below).
    // настройки тени (см. ниже).
    "shadow": {}
  },
  "playersPanel": {
    // Opacity percentage of the panels. 0 - transparent, 100 - opaque.
    // Прозрачность в процентах ушей. 0 - прозрачные, 100 - не прозрачные.
    "alpha": 50,
    // Opacity percentage of icons in the panels. 0 - transparent ... 100 - opaque.
    // Прозрачность в процентах иконок в ушах. 0 - прозрачные, 100 - не прозрачные.
    "iconAlpha": 100,
    // true - Disable Platoon icons.
    // true - убрать отображение иконки взвода.
    "removeSquadIcon": false,
    // true - disable background of the selected player.
    // true - убрать подложку выбранного игрока.
    "removeSelectedBackground": false,
    // true - Remove the Players Panel mode switcher (buttons for changing size).
    // true - убрать переключатель режимов ушей мышкой.
    "removePanelsModeSwitcher": false,
    // Start panels mode. Possible values: "none", "short", "medium", "medium2", "large".
    // Начальный режим ушей. Возможные значения: "none", "short", "medium", "medium2", "large".
    "startMode": "medium",
    // Alternative panels mode. Possible values: null, "none", "short", "medium", "medium2", "large".
    // Альтернативный режим ушей. Возможные значения: null, "none", "short", "medium", "medium2", "large".
    "altMode": null,
    // Display options for Team/Clan logos (see battleLoading.xc).
    // Параметры отображения иконки игрока/клана (см. battleLoading.xc).
    "clanIcon": {
      "show": false,
      "x": 82,
      "y": 3,
      "xr": 82,
      "yr": 6,
      "w": 16,
      "h": 16,
      "alpha": 90
    },
    // Options for the "none" panels - empty panels.
    // Режим ушей "none" - пустые уши.
    "none": {
      // false - disable (отключить)
      "enabled": true,
      // Layout ("vertical" or "horizontal")
      // Размещение ("vertical" - вертикально, или "horizontal" - горизонтально)
      "layout": "vertical",
      // Extra fields.
      // Дополнительные  поля.
      "extraFields": {
        "leftPanel": {
          "x": 0, // from left side of screen
          "y": 65,
          "width": 350,
          "height": 25,
          // Set of formats for left panel
          // Набор форматов для левой панели
          // example:
          // "format": [
          //   // simple format (just a text)
          //   "{{nick}}",
          //   // extended format
          //   { "x": 20, "y": 10, "border": 1, "borderColor": "0xFFFFFF", "format": "{{nick}}" }
          //   // fields available for extended format (all fields are optional):
          //   //   "format"
          //   //
          //   //   "x" (macros allowed)
          //   //   "y" (macros allowed)
          //   //   "w" (macros allowed)
          //   //   "h" (macros allowed)
          //   //   "alpha" (0..100) (macros allowed)
          //   //   "rotation" (0..360) (macros allowed)
          //   //
          //   //   "align" (left(*default for left panel), center, right(*default for right panel))
          //   //   "valign" (top(*), bottom, center)
          //   //   "borderColor" (macros allowed)
          //   //   "bgColor" (macros allowed)
          //   //   "antiAliasType" (advanced(*) or normal
          //   //   "shadow": {
          //   //     "distance" (in pixels)
          //   //     "angle"    (0.0 .. 360.0)
          //   //     "color"
          //   //     "alpha"    (0.0 .. 1.0)
          //   //     "blur"     (0.0 .. 255.0)
          //   //     "strength" (0.0 .. 255.0)
          //   //    }
          // ]
          "formats": []
        },
        "rightPanel": {
          "x": 0, // from right side of screen
          "y": 65,
          "width": 350,
          "height": 25,
          // Set of formats for right panel
          // Набор форматов для правой панели
          "formats": [
        // enemy spotted status marker (see above).
        // маркер статуса засвета (см. выше).
        ${"enemySpottedMarker"}
		]
        }
      }
    },
    // Options for the "short" panels - panels with frags and vehicle icon.
    // Режим ушей "short" - короткие уши (фраги и иконка танка).
    "short": {
      // false - disable (отключить)
      "enabled": true,
      // Width of the column, 0-250. Default is 0.
      // Ширина поля, 0-250. По умолчанию: 0.
      "width": 0,
      // Display format for frags (macros allowed, see readme-en.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. readme-ru.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each text field have size 350x25. Fields are placed one above the other.
      // Дополнительные  поля. Каждое поле имеет размер 350x25. Поля располагаются друг над другом.
      // Set of formats for left panel
      // Набор форматов для левой панели
      "extraFieldsLeft": [
    { "x": 0, "y": 0, "w": 26, "h": 24, "valign": "center", "src": "cfg://Ded_Shalfey/img/{{ready?|A-noready}}{{player}}{{squad}}.png", "alpha": "{{alive?35|0}}" }, 
    { "x": 180, "y": 2, "valign": "center", "w": 60, "h": 22, "bgColor": "0x000000", "alpha": "{{alive?50|0}}" },
    { "x": 182, "y": 4, "valign": "center", "w": "{{hp-ratio:56}}", "h": 18, "bgColor": "{{c:system}}", "alpha": "{{alive?50|0}}" },
    { "x": 210, "align": "center", "valign": "center", "format": "<font size='12'><b>{{hp%4.4s|-----}}</b></font>", "alpha": "{{alive?100|0}}", "shadow": { "distance": 0, "angle": 90, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 2 } }
  ],
      // Set of formats for right panel
      // Набор форматов для правой панели
      "extraFieldsRight": [
    { "x": 0, "y": 0, "w": 26, "h": 24, "valign": "center", "src": "cfg://Ded_Shalfey/img/{{ready?|E-noready}}.png", "alpha": "{{alive?35|0}}" },  
    { "x": 180, "y": 2, "valign": "center", "w": 60, "h": 22, "bgColor": "0x000000", "alpha": "{{alive?50|0}}" },
    { "x": 182, "y": 4, "valign": "center", "w": "{{hp-ratio:56}}", "h": 18, "bgColor": "{{c:system}}", "alpha": "{{alive?50|0}}" },
    { "x": 210, "align": "center", "valign": "center", "format": "<font size='12'><b>{{hp%4.4s|-----}}</b></font>", "alpha": "{{alive?100|0}}", "shadow": { "distance": 0, "angle": 90, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 2 } },
        // enemy spotted status marker (see above).
        // маркер статуса засвета (см. выше).
        ${"enemySpottedMarker"}
		]
    },
    // Options for the "medium" panels - the first of the medium panels.
    // Режим ушей "medium" - первые средние уши в игре.
    "medium": {
      // false - disable (отключить)
      "enabled": true,
      // Width of the player's name column, 0-250. Default is 46.
      // Ширина поля имени игрока, 0-250. По умолчанию: 46.
      "width": 90,
      // Display format for the left panel (macros allowed, see readme-en.txt).
      // Формат отображения для левой панели (допускаются макроподстановки, см. readme-ru.txt).
      "formatLeft": "<font face='$TextFont' size='12' color='{{c:r}}' alpha='{{alive?#FF|#80}}'> {{nick%.12s~..}}</font><font face='Consolas' size='12'> </font>",
      // Display format for the right panel (macros allowed, see readme-en.txt).
      // Формат отображения для правой панели (допускаются макроподстановки, см. readme-ru.txt).
      "formatRight": "<font face='Consolas' size='12'> </font><font face='$TextFont' size='12' color='{{c:r}}' alpha='{{alive?#FF|#80}}'>{{nick%.12s~..}} </font>",
      // Display format for frags (macros allowed, see readme-en.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. readme-ru.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each text field have size 350x25. Fields are placed one above the other.
      // Дополнительные  поля. Каждое поле имеет размер 350x25. Поля располагаются друг над другом.
      // Set of formats for left panel
      // Набор форматов для левой панели
      "extraFieldsLeft": [],
      // Set of formats for right panel
      // Набор форматов для правой панели
      "extraFieldsRight": [
        // enemy spotted status marker (see above).
        // маркер статуса засвета (см. выше).
        ${"enemySpottedMarker"}
		]
    },
    // Options for the "medium2" panels - the second of the medium panels.
    // Режим ушей "medium2" - вторые средние уши в игре.
    "medium2": {
      // false - disable (отключить)
      "enabled": true,
      // Width of the vehicle name column, 0-250. Default is 65.
      // Ширина поля названия танка, 0-250. По умолчанию: 65.
      "width": 70,
      // Display format for the left panel (macros allowed, see readme-en.txt).
      // Формат отображения для левой панели (допускаются макроподстановки, см. readme-ru.txt).
      "formatLeft": "<font color='{{c:rating}}' alpha='{{alive?#FF|#80}}'>{{vehicle}}</font>",
      // Display format for the right panel (macros allowed, see readme-en.txt).
      // Формат отображения для правой панели (допускаются макроподстановки, см. readme-ru.txt).
      "formatRight": "<font color='{{c:rating}}' alpha='{{alive?#FF|#80}}'>{{vehicle}}</font>",
      // Display format for frags (macros allowed, see readme-en.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. readme-ru.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each text field have size 350x25. Fields are placed one above the other.
      // Дополнительные  поля. Каждое поле имеет размер 350x25. Поля располагаются друг над другом.
      // Set of formats for left panel
      // Набор форматов для левой панели
      "extraFieldsLeft": [],
      // Set of formats for right panel
      // Набор форматов для правой панели
      "extraFieldsRight": [
        // enemy spotted status marker (see above).
        // маркер статуса засвета (см. выше).
        ${"enemySpottedMarker"}
		]
    },
    // Options for the "large" panels - the widest panels.
    // Режим ушей "large" - широкие уши в игре.
    "large": {
      // false - disable (отключить)
      "enabled": true,
      // Width of the player's name column, 0-250. Default is 170.
      // Ширина поля имени игрока, 0-250. По умолчанию: 170.
      "width": 75,
      // Display format for player nickname (macros allowed, see readme-en.txt).
      // Формат отображения имени игрока (допускаются макроподстановки, см. readme-ru.txt).
      "nickFormatLeft": "<font face='$TextFont' size='12' color='{{c:r}}' alpha='{{alive?#FF|#80}}'>{{nick%.10s~..}}</font><font face='Consolas' size='12'> </font>",
      "nickFormatRight": "<font face='Consolas' size='12'> </font><font face='$TextFont' size='12' color='{{c:r}}' alpha='{{alive?#FF|#80}}'>{{nick%-.10s~..}}</font>",
      // Display format for vehicle name (macros allowed, see readme-en.txt).
      // Формат отображения названия танка (допускаются макроподстановки, см. readme-ru.txt).
      "vehicleFormatLeft": "<textformat tabstops='[25]'><font color='{{c:kb}}' alpha='{{alive?#FF|#80}}'>{{kb%2d~k|--k}}</font><tab><font color='{{c:rating}}' alpha='{{alive?#FF|#80}}'>{{rating%2d~%|--%}}</font></textformat>",
      "vehicleFormatRight": "<textformat tabstops='[25]'><font color='{{c:kb}}' alpha='{{alive?#FF|#80}}'>{{kb%2d~k|--k}}</font><tab><font color='{{c:rating}}' alpha='{{alive?#FF|#80}}'>{{rating%2d~%|--%}}</font></textformat>",
      // Display format for frags (macros allowed, see readme-en.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. readme-ru.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each text field have size 350x25. Fields are placed one above the other.
      // Дополнительные  поля. Каждое поле имеет размер 350x25. Поля располагаются друг над другом.
      // Set of formats for left panel
      // Набор форматов для левой панели
      "extraFieldsLeft": [],
      // Set of formats for right panel
      // Набор форматов для правой панели
      "extraFieldsRight": [
        // enemy spotted status marker (see above).
        // маркер статуса засвета (см. выше).
        ${"enemySpottedMarker"}
		]
    }
  }
}
